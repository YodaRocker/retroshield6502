# RetroShield 6502 for Arduino Mega

I'm excited to announce a very cool project for retromaniacs:

* RetroShield 6502 is a hardware shield for Arduino Mega. 
* It enables Arduino to emulate peripherals and run 6502 programs using a real 6502! 
* You can try 6502 assembly and run some of the old 6502 programs 
without building complicated circuits. 
* It supports 6502 variants such as 65C02 and W65C02S. 
* You can use existing Arduino shields to extend the capabilities. 
* Arduino Mega gives you 4~6KB of RAM and >200KB ROM (in flash area).
* All hardware and software files are provided for you to study and play. 
* An *Apple I setup* is included, in honor of the pioneers in that time frame. 
* Hardware details and PCB/kits forsale: http://www.8bitforce.com/

### Installation

First:
* DO NOT PLUG RETROSHIELD before programming Arduino first!

Otherwise any prexisting code may drive GPIO's randomly and short-circuit
your precious microprocessor.

Available Hardware Platforms:

* `k65c02_apple1`: emulates Apple I hardware, includes BIOS and BASIC ROMs.
* `k65c02_c64`: emulates C64 hardware to run BASIC thru VT100.
* `k65c02_kim1`: emulates KIM-1 hardware thru TTY. (limited testing.)


To run Apple I, you need:
* Arduino Mega
* LCD Display Shield for Arduino

0) Disconnect RetroShield from Arduino Mega.
1) Program Arduino Mega with Apple I code (software/k6502_apple1/).
2) Disconnect Arduino Mega from computer and plug RetroShield 6502.
3) Connect Arduino Mega to computer.
4) Open `Serial Monitor` from Arduino IDE `Tools` menu.
5) Select `115200` for Baud and `Carriage Return` for line endings.
6) Press DOWN buttons to assert CPU RESET.
7) Press UP button to release CPU RESET.
8) You should see prompt `\` in serial monitor now.
9) type `e000.e100` + enter to see memory dump from 0xE000 to 0xE100.
10) Optional: type `E000R` to run the Apple I Basic ROM :)

![RetroShield in Action](/docs/k65c02_apple1_basic.png)

### 6502 Links

* http://www.6502.org/
* http://www.callapple.org/soft/ap1/emul.html
* SB-Assembler: https://www.sbprojects.net/sbasm/

### License Information

* Copyright 2019 Erturk Kocalar, 8Bitforce.com
* Hardware is released under [Creative Commons ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/).
* Software is released under the [MIT License](http://opensource.org/licenses/MIT).
* Distributed as-is; no warranty is given.
* See LICENSE.md for details.
* All text above must be included in any redistribution.
